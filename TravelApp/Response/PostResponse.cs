﻿using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;
using TravelApp.Response;
using TravelApp.Dto;
using System.Collections.Generic;
using System;

namespace TravelApp.Response
{
    public class PostResponse
    {
        public Post post { get; set; }
        public string nameUser { get; set; }
        public string avatarUser { get; set; }
        public int likeCount { get; set; }
        public int dislikeCount { get; set; }
        public int commentCount { get; set; }
        public bool isLiked { get; set; }
        public bool isDisliked { get; set; }
        public int timeSub { get; set; }
        public int typeTimePost { get; set; }
        public List<CommentResponse> listCommentResponse { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="post"></param>
        /// <param name="idUser"></param>
        public PostResponse(Post post, int idUser)
        {
            this.Constructor();

            this.post = post;
            this.nameUser = new UserRepository(new EntityContext()).GetEntityById(post.idUser).userName;
            this.avatarUser = new UserRepository(new EntityContext()).GetEntityById(post.idUser).avatarImage;
            this.likeCount = new LikePostRepository(new EntityContext()).GetLikeCount(post.id);
            this.dislikeCount = new DislikePostRepository(new EntityContext()).GetDislikeCount(post.id);
            this.commentCount = new CommentRepository(new EntityContext()).GetCommentParentByIdPost(post.id).Count;
            this.isLiked = (new LikePostRepository(new EntityContext()).LikePostExist(post.id, idUser) != 0);
            this.isDisliked = (new DislikePostRepository(new EntityContext()).DislikePostExist(post.id, idUser) != 0);
            
            foreach (var comment in new CommentRepository(new EntityContext()).GetCommentParentByIdPost(post.id))
            {
                CommentResponse commentResponse = new CommentResponse(comment, idUser);
                this.listCommentResponse.Add(commentResponse);
            }

            this.SetTimeResponse();
        }

        public PostResponse() { }

        /// <summary>
        /// Set Default Value
        /// </summary>
        public void Constructor() {
            this.post = new Post();
            this.nameUser = "";
            this.avatarUser = "";
            this.likeCount = 0;
            this.dislikeCount = 0;
            this.commentCount = 0;
            this.isLiked = false;
            this.isDisliked = false;
            this.timeSub = 0;
            this.typeTimePost = 0;
            this.listCommentResponse = new List<CommentResponse>();
        }

        /// <summary>
        /// Set Time of Post (View)
        /// </summary>
        public void SetTimeResponse()
        {
            // this.timeSpan = DateTime.Now.Subtract(this.post.timeCreated);
            if (DateTime.Now.Subtract(this.post.timeCreated).Days > 6)
            {
                this.typeTimePost = 4;
                this.timeSub = DateTime.Now.Subtract(this.post.timeCreated).Days;
            }
            else
            {
                if (DateTime.Now.Subtract(this.post.timeCreated).Days > 1 && DateTime.Now.Subtract(this.post.timeCreated).Days <= 6)
                {
                    this.timeSub = DateTime.Now.Subtract(this.post.timeCreated).Days;
                    this.typeTimePost = 3;
                }
                else
                {
                    if (DateTime.Now.Subtract(this.post.timeCreated).Hours > 0)
                    {
                        this.timeSub = DateTime.Now.Subtract(this.post.timeCreated).Hours;
                        this.typeTimePost = 2;
                    }
                    else
                    {
                        this.timeSub = DateTime.Now.Subtract(this.post.timeCreated).Minutes;
                        this.typeTimePost = 1;
                    }
                }
            }
        }
    }
}
