﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;
using TravelApp.Response;
using TravelApp.Dto;

namespace TravelApp.Response
{
    public class ProfileResponse
    {
        public UserDto User { get; set; }
        public int Followers { get; set; }
        public int Following { get; set; }
        public int countPosts { get; set; }
        public bool isFollowing { get; set; }
        public List<PostResponse> listPostResponses { get; set; }
        public List<FollowResponse> listFollowers { get; set; }
        public List<FollowResponse> listFollowing { get; set; }
        public string listMedia { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="idUserProfile"></param>
        /// <param name="idUser"></param>
        public ProfileResponse(int idUserProfile, int idUser)
        {
            this.SetDefaultValue();
            EntityContext context = new EntityContext();
            UserRepository userRepository = new UserRepository(context);
            FollowRepository followRepository = new FollowRepository(context);
            PostRepository postRepository = new PostRepository(context);


            this.User = new UserDto(userRepository.GetEntityById(idUserProfile));
            this.Followers = followRepository.GetListIdFollower(idUserProfile).Count();
            this.Following = followRepository.GetListIdFollowing(idUserProfile).Count();
            this.countPosts = postRepository.GetByIdUser(idUserProfile).Count();
            this.isFollowing = (followRepository.FollowExist(idUser, idUserProfile) != 0);
            this.listMedia = postRepository.GetListMedia(idUserProfile);

            foreach (Post post in postRepository.GetByIdUser(idUserProfile))
            {
                PostResponse postResponses = new PostResponse(post, idUser);
                postResponses.listCommentResponse = null;  
                this.listPostResponses.Add(postResponses);
            }

            foreach(int id in followRepository.GetListIdFollower(idUserProfile))
            {
                this.listFollowers.Add(new FollowResponse(id));
            }

            foreach (int id in followRepository.GetListIdFollowing(idUserProfile))
            {
                this.listFollowing.Add(new FollowResponse(id));
            }
        }

        public ProfileResponse() { }

        /// <summary>
        /// Set Default Value
        /// </summary>
        public void SetDefaultValue()
        {
            this.User = new UserDto();
            this.Followers = 0;
            this.Following = 0;
            this.countPosts = 0;
            this.isFollowing = false;
            this.listPostResponses = new List<PostResponse>();
            this.listFollowers = new List<FollowResponse>();
            this.listFollowing = new List<FollowResponse>();
        }
    }
}
