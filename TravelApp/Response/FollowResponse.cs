﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;
using TravelApp.Response;
using TravelApp.Dto;

namespace TravelApp.Response
{
    public class FollowResponse
    {
        public int idUser { get; set; }
        public string avatarImage { get; set; }
        public string userName { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="idUser"></param>
        public FollowResponse (int idUser)
        {
            this.SetDefaultValue();
            this.idUser = idUser;
            this.avatarImage = new UserRepository(new EntityContext()).GetEntityById(idUser).avatarImage;
            this.userName = new UserRepository(new EntityContext()).GetEntityById(idUser).userName;
        }

        /// <summary>
        /// Set Default Value
        /// </summary>
        public void SetDefaultValue()
        {
            this.idUser = 0;
            this.avatarImage = "";
            this.userName = "";
        }
    }
}
