﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;
using TravelApp.Response;
using TravelApp.Dto;

namespace TravelApp.Response
{
    public class NotificationResponse
    {
        public Notification notification { get; set; }
        public string nameUserB { get; set; }
        public string avatarUserB { get; set; }
        public int timeSub { get; set; }
        public int typeList { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="notification"></param>
        public NotificationResponse(Notification notification, int idUser)
        {
            this.notification = notification;
            this.nameUserB = new UserRepository(new EntityContext()).GetEntityById(notification.idUserB).userName;
            this.avatarUserB = new UserRepository(new EntityContext()).GetEntityById(notification.idUserB).avatarImage;
            this.timeSub = 0;
            this.typeList = 0;

            if (DateTime.Now.Subtract(notification.timeCreated).Days > 6)
            {
                this.typeList = 4;
                this.timeSub = DateTime.Now.Subtract(notification.timeCreated).Days;
            } else
            {
                if (DateTime.Now.Subtract(notification.timeCreated).Days > 1 && DateTime.Now.Subtract(notification.timeCreated).Days <= 6)
                {
                    this.timeSub = DateTime.Now.Subtract(notification.timeCreated).Days;
                    this.typeList = 3;
                } else
                {
                    if(DateTime.Now.Subtract(notification.timeCreated).Hours > 0)
                    {
                        this.timeSub = DateTime.Now.Subtract(notification.timeCreated).Hours;
                        this.typeList = 2;
                    } else
                    {
                        this.timeSub = DateTime.Now.Subtract(notification.timeCreated).Minutes;
                        this.typeList = 1;
                    }
                }
            }
        }

        public NotificationResponse() { }
    }

}
