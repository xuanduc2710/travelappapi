﻿using System.Collections.Generic;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;

namespace TravelApp.Response
{
    public class NotificationScreenResponse
    {
        public List<NotificationResponse> listInHour { get; set; }
        public List<NotificationResponse> listInDay { get; set; }
        public List<NotificationResponse> listInWeek { get; set; }
        public List<NotificationResponse> listRest { get; set; }

        /// <summary>
        /// Constructor with Lists
        /// </summary>
        /// <param name="listInHour"></param>
        /// <param name="listInDay"></param>
        /// <param name="listInWeek"></param>
        /// <param name="listRest"></param>
        public NotificationScreenResponse(List<NotificationResponse> listInHour, List<NotificationResponse> listInDay, List<NotificationResponse> listInWeek, List<NotificationResponse> listRest)
        {
            this.listInHour = listInHour;
            this.listInDay = listInDay;
            this.listInWeek = listInWeek;
            this.listRest = listRest;
        }

        /// <summary>
        /// Constructor with listNotificationResponses
        /// </summary>
        /// <param name="listNotificationResponses"></param>
        public NotificationScreenResponse(int idUser)
        {
            this.SetDefaultValue();

            List<NotificationResponse> listNotificationResponses = new List<NotificationResponse>();
            foreach (Notification notification in new NotificationRepository(new EntityContext()).GetByIdUser(idUser))
            {
                listNotificationResponses.Add(new NotificationResponse(notification, idUser));
            }

            foreach (NotificationResponse response in listNotificationResponses)
            {
                switch (response.typeList)
                {
                    case 1:
                        this.listInHour.Add(response);
                        break;
                    case 2:
                        this.listInDay.Add(response);
                        break;
                    case 3:
                        this.listInWeek.Add(response);
                        break;
                    case 4:
                        this.listRest.Add(response);
                        break;
                }
            }
        }

        /// <summary>
        /// Set Default value
        /// </summary>
        public void SetDefaultValue()
        {
            this.listInHour = new List<NotificationResponse>();
            this.listInDay = new List<NotificationResponse>();
            this.listInWeek = new List<NotificationResponse>();
            this.listRest = new List<NotificationResponse>();
        }

        public NotificationScreenResponse() { }
    }
}
