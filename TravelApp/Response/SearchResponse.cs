﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;
using TravelApp.Response;
using TravelApp.Dto;

namespace TravelApp.Response
{
    public class SearchResponse
    {
        public List<PostResponse> listPostSearch { get; set; }
        public List<FollowResponse> listUserSearch { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="keySearch"></param>
        /// <param name="typeSearch"></param>
        /// <param name="idUser"></param>
        /// <param name="page"></param>
        public SearchResponse(string keySearch, int idUser, int pageUser, int pagePost)
        {
            this.listPostSearch = new List<PostResponse>();
            this.listUserSearch = new List<FollowResponse>();

            this.listPostSearch = GetPostSearch(keySearch, idUser, pagePost);
            this.listUserSearch = GetUserSearch(keySearch, pageUser, idUser);
        }

        /// <summary>
        /// Get List Post Response Searched
        /// </summary>
        /// <param name="keySearch"></param>
        /// <param name="idUser"></param>
        /// <param name="pagePost"></param>
        /// <returns></returns>
        public List<PostResponse> GetPostSearch(string keySearch, int idUser, int pagePost)
        {
            if (pagePost == 0) return new List<PostResponse>();
            List<Post> listPostTemp = new PostRepository(new EntityContext()).GetByKeyString(keySearch);
            List<PostResponse> listAll = new List<PostResponse>();

            for (int i = 0; i < listPostTemp.Count() && listAll.Count() <= (pagePost * 5); i++)
            {
                if (listPostTemp[i].idUser == idUser) continue;
                PostResponse postResponse = new PostResponse(listPostTemp[i], idUser);
                postResponse.listCommentResponse = new List<CommentResponse>();
                listAll.Add(postResponse);
            }

            return listAll.GetRange(pagePost * 5 - 5, ((listAll.Count()) <= (pagePost * 5)) ? (listAll.Count()) : (pagePost * 5));
        }

        /// <summary>
        /// Get List User Response Searched
        /// </summary>
        /// <param name="keySearch"></param>
        /// <param name="pageUser"></param>
        /// <returns></returns>
        public List<FollowResponse> GetUserSearch(string keySearch, int pageUser, int idUser)
        {
            if (pageUser == 0) return new List<FollowResponse>();
            List<User> listUserTemp = new UserRepository(new EntityContext()).GetByKeyString(keySearch);
            List<FollowResponse> listAll = new List<FollowResponse>();

            for (int i = 0; i < listUserTemp.Count() && listAll.Count() <= (pageUser * 5); i++)
            {
                if (listUserTemp[i].id == idUser) continue;
                listAll.Add(new FollowResponse(listUserTemp[i].id));
            }

            return listAll.GetRange(pageUser * 5 - 5, ((listAll.Count()) <= (pageUser * 5)) ? (listAll.Count()) : (pageUser * 5));
        }
    }
}
