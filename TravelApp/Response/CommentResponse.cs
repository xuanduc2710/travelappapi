﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;
using TravelApp.Response;
using TravelApp.Dto;

namespace TravelApp.Response
{
    public class CommentResponse
    {
        public Comment comment { get; set; }
        public string nameUser { get; set; }
        public string avatarUser { get; set; }
        public int likeCommentCount { get; set; }
        public bool isLiked { get; set; }
        public int timeSub { get; set; }
        public int typeTimePost { get; set; }
        public List<CommentResponse> listChildComment { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="likeCommentCount"></param>
        /// <param name="isLiked"></param>
        public CommentResponse(Comment comment, int idUser)
        {
            this.SetDefaultValue();

            this.nameUser = new UserRepository(new EntityContext()).GetEntityById(comment.idUser).userName;
            this.avatarUser = new UserRepository(new EntityContext()).GetEntityById(comment.idUser).avatarImage;
            this.comment = comment;
            this.likeCommentCount = new LikeCommentRepository(new EntityContext()).GetLikeCount(comment.id);
            this.isLiked = (new LikeCommentRepository(new EntityContext()).LikeCommentExist(comment.id, idUser) != 0);
            this.SetTimeResponse();
            this.SetListChildComment(idUser);
        }

        public CommentResponse() { }

        /// <summary>
        /// Set Default Value
        /// </summary>
        public void SetDefaultValue()
        {
            this.comment = new Comment();
            this.nameUser = "";
            this.avatarUser = "";
            this.likeCommentCount = 0;
            this.isLiked = false;
            this.timeSub = 0;
            this.typeTimePost = 0;
            this.listChildComment = new List<CommentResponse>();
        }

        /// <summary>
        /// Set Time Response
        /// </summary>
        public void SetTimeResponse()
        {
            if (DateTime.Now.Subtract(comment.timeCreated).Days > 6)
            {
                this.typeTimePost = 4;
                this.timeSub = DateTime.Now.Subtract(comment.timeCreated).Days;
            }
            else
            {
                if (DateTime.Now.Subtract(comment.timeCreated).Days > 1 && DateTime.Now.Subtract(comment.timeCreated).Days <= 6)
                {
                    this.timeSub = DateTime.Now.Subtract(comment.timeCreated).Days;
                    this.typeTimePost = 3;
                }
                else
                {
                    if (DateTime.Now.Subtract(comment.timeCreated).Hours > 0)
                    {
                        this.timeSub = DateTime.Now.Subtract(comment.timeCreated).Hours;
                        this.typeTimePost = 2;
                    }
                    else
                    {
                        this.timeSub = DateTime.Now.Subtract(comment.timeCreated).Minutes;
                        this.typeTimePost = 1;
                    }
                }
            }
        }

        /// <summary>
        /// Set Value List ChileComment
        /// </summary>
        /// <param name="idUser"></param>
        public void SetListChildComment(int idUser)
        {
            foreach (var commentResponse in new CommentRepository(new EntityContext()).GetChildComment(comment.id))
            {
                CommentResponse c = new CommentResponse(commentResponse, idUser);
                c.listChildComment = new List<CommentResponse>();
                this.listChildComment.Add(c);
            }
        }
    }
}
