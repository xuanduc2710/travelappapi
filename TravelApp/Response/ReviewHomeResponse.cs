﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;
using TravelApp.Response;
using TravelApp.Dto;

namespace TravelApp.Response
{
    public class ReviewHomeResponse
    {
        public List<PostResponse> listPostResponses { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="idUser"></param>
        /// <param name="page"></param>
        public ReviewHomeResponse(int idUser, int page)
        {
            this.listPostResponses = new List<PostResponse>();

            List<Post> listTemp = new PostRepository(new EntityContext()).GetAllEntity().OrderBy(o => o.timeCreated).Reverse().ToList();
            for (int i = page * 5 - 5; i < listTemp.Count() && i < page * 5; i++)
            {
                PostResponse postResponse = new PostResponse(listTemp[i], idUser);
                postResponse.listCommentResponse = new List<CommentResponse>();
                this.listPostResponses.Add(postResponse);
            }
        }

        public ReviewHomeResponse() { }
    }
}
