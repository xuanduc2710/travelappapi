﻿using TravelApp.Data;
using TravelApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(EntityContext ctx) : base(ctx)
        {
        }
        
        /// <summary>
        /// Check Login
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        public User CheckLogin(UserLogin userLogin)
        {
            User user = null;
            user = GetAllEntity().FirstOrDefault(a => a.email.ToLower().Equals(userLogin.email) && a.password.Equals(userLogin.password));
            return user;
        }

        /// <summary>
        /// Check Account Exist
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool CheckAccountExist(string email)
        {
            var arr = Model.Where(u => u.email == email);
            if (arr.Any()) { return true; }
            return false;
        }

        /// <summary>
        /// Search User By keySearch
        /// </summary>
        /// <param name="keySearch"></param>
        /// <returns></returns>
        public List<User> GetByKeyString(string keySearch)
        {
            return Model.Where(user => user.userName.ToLower().Contains(keySearch.ToLower())).ToList();
        }
    }
}