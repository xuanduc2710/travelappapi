﻿using TravelApp.Data;
using TravelApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Repositories
{
    public class DislikePostRepository : BaseRepository<DislikePost>
    {
        public DislikePostRepository(EntityContext ctx) : base(ctx)
        {
        }

        /// <summary>
        /// Check DislikePost Exits by idPost & idUser
        /// </summary>
        /// <param name="idPost"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public int DislikePostExist(int idPost, int idUser)
        {
            var arr = Model.Where(dislikePost => dislikePost.idPost == idPost && dislikePost.idUser == idUser).ToList();
            if (arr.Any()) { 
                return arr.ElementAt(0).id; 
            }
            return 0;
        }


        /// <summary>
        /// Get DislikeCount of Post
        /// </summary>
        /// <param name="idPost"></param>
        /// <returns></returns>
        public int GetDislikeCount(int idPost)
        {
            return Model.Count(disLikePost => disLikePost.idPost == idPost);
        }

        /// <summary>
        /// Delete All DislikePost of a Post by IdPost When a Post is deleted
        /// </summary>
        /// <param name="idPost"></param>
        public void DeleteByIdPost(int idPost)
        {
            foreach (var c in Model.Where(dislikePost => dislikePost.idPost == idPost))
            {
                DeleteEntityById(c.id);
            }
        }
    }
}
