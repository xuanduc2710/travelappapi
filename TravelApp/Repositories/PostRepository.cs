﻿using TravelApp.Data;
using TravelApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Repositories
{
    public class PostRepository : BaseRepository<Post>
    {
        public PostRepository(EntityContext ctx) : base(ctx)
        {
        }

        /// <summary>
        /// Increase ShareCount
        /// </summary>
        /// <param name="idPost"></param>
        /// <returns></returns>
        public Post IncreaseShareCount(int idPost)
        {
            Post post = new Post();
            post = GetEntityById(idPost);
            post.shareCount += 1;
            UpdateEntity(idPost, post);
            return post;
        }

        /// <summary>
        /// Get list post by idUser
        /// </summary>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public List<Post> GetByIdUser (int idUser)
        {
            return Model.Where(post => post.idUser == idUser).OrderBy(p => p.timeCreated).Reverse().ToList();
        }

        /// <summary>
        /// Get List Post By Keysearch
        /// </summary>
        /// <param name="keySearch"></param>
        /// <returns></returns>
        public List<Post> GetByKeyString (string keySearch)
        {
            return Model.Where(post => post.title.ToLower().Contains(keySearch.ToLower()) ||
                                        post.content.ToLower().Contains(keySearch.ToLower()) ||
                                        post.location.ToLower().Contains(keySearch.ToLower())).OrderBy(p => p.timeCreated).Reverse().ToList();
        }

        /// <summary>
        /// Get List Media of a User
        /// </summary>
        /// <param name="idUserProfile"></param>
        /// <returns></returns>
        public string GetListMedia(int idUserProfile)
        {
            var arr = from c in Model.Where(p => p.idUser == idUserProfile && p.images != "")
                      select c.images;
            return String.Join(",", arr);
        }
    }
}
