﻿using TravelApp.Data;
using TravelApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Repositories
{
    public class NotificationRepository : BaseRepository<Notification>
    {
        public NotificationRepository(EntityContext ctx) : base(ctx)
        {
        }

        /// <summary>
        /// Check Notification Exist
        /// </summary>
        /// <param name="idUserB"></param>
        /// <param name="typeNotify"></param>
        /// <param name="idPost"></param>
        /// <param name="idComment"></param>
        /// <returns></returns>
        public int NotificationExist(int idUser, int idUserB, int typeNotify, int idPost, int idComment)
        {
            var arr = Model.Where(notification => notification.idUserB == idUserB &&
                                    notification.idUser == idUser &&
                                    notification.typeNotify == typeNotify && 
                                    notification.idPost == idPost && 
                                    notification.idComment == idComment).ToList();
            if (arr.Any()) { return arr.ElementAt(0).id; }
            return 0;
        }

        /// <summary>
        /// Get List Notification of a User by IdUser
        /// </summary>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public List<Notification> GetByIdUser (int idUser)
        {
            return Model.Where(c => c.idUser == idUser && c.idUserB != idUser).ToList().OrderBy(n => n.timeCreated).Reverse().ToList();
        }

        /// <summary>
        /// Delte notification when delete a comment
        /// </summary>
        /// <param name="idComment"></param>
        public void DeleteByIdComment(int idComment)
        {
            foreach (var c in Model.Where(not => not.idComment == idComment))
            {
                DeleteEntityById(c.id);
            }
        }

        /// <summary>
        /// Delte notification when delete a post
        /// </summary>
        /// <param name="idPost"></param>
        public void DeleteByIdPost(int idPost)
        {
            foreach (var c in Model.Where(not => not.idPost == idPost))
            {
                DeleteEntityById(c.id);
            }
        }

        /// <summary>
        /// Delete When Unfollow
        /// </summary>
        /// <param name="idFollower"></param>
        /// <param name="idFollowed"></param>
        public void DeleteWhenUnFollow(int idFollower, int idFollowed)
        {
            foreach (var c in Model.Where(not => not.idUser == idFollower && not.idUserB == idFollowed && (not.typeNotify == 6)))
            {
                DeleteEntityById(c.id);
            }
        }
    }
}
