﻿using TravelApp.Data;
using TravelApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Repositories
{
    public class LikeCommentRepository : BaseRepository<LikeComment>
    {
        private readonly EntityContext context;
        public LikeCommentRepository(EntityContext ctx) : base(ctx)
        {
        }

        /// <summary>
        /// Get LikeCount of a Comment
        /// </summary>
        /// <param name="idComment"></param>
        /// <returns></returns>
        public int GetLikeCount(int idComment)
        {
            return Model.Where(likeCom => likeCom.idComment == idComment).Count();
        }

        /// <summary>
        /// Check Comment Exist
        /// </summary>
        /// <param name="idComment"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public int LikeCommentExist(int idComment, int idUser)
        {
            var arr = Model.Where(likeComment => likeComment.idComment == idComment && likeComment.idUser == idUser).ToList();
            if (arr.Any()) { return arr.ElementAt(0).id; }
            return 0;
        }

        /// <summary>
        /// Delete LikeComment when a Comment's deleted
        /// </summary>
        /// <param name="idComment"></param>
        public void DeleteByIdComment(int idComment)
        {
            foreach (var c in Model.Where(c => c.idComment == idComment))
            {
                DeleteEntityById(c.id);
            }
        }
    }
}
