﻿using TravelApp.Data;
using TravelApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Repositories
{
    public class FollowRepository : BaseRepository<Follow>
    {
        public FollowRepository(EntityContext ctx) : base(ctx)
        {
        }

        /// <summary>
        /// Check Follow Exist by idFollower & idFollowed
        /// </summary>
        /// <param name="idFollower"></param>
        /// <param name="idFollowed"></param>
        /// <returns></returns>
        public int FollowExist(int idFollower, int idFollowed)
        {
            var arr = Model.Where(f => f.idFollower == idFollower && f.idFollowed == idFollowed).ToList();
            if (arr.Any()) { return arr.ElementAt(0).id; }
            return 0;
        }

        /// <summary>
        /// Get List Follower
        /// </summary>
        /// <param name="idFollowed"></param>
        /// <returns></returns>
        public List<int> GetListIdFollower(int idFollowed)
        {
            var arr = from c in Model.Where(c => c.idFollowed == idFollowed).ToList().OrderBy(f => f.id).Reverse().ToList()
                      select c.idFollower;
            return arr.ToList();
        }

        /// <summary>
        /// Get List Following
        /// </summary>
        /// <param name="idFollower"></param>
        /// <returns></returns>
        public List<int> GetListIdFollowing(int idFollower)
        {
            var arr = from c in Model.Where(c => c.idFollower == idFollower).ToList().OrderBy(f => f.id).Reverse().ToList()
                      select c.idFollowed;
            return arr.ToList();
        }
    }
}
