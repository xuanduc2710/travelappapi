﻿using TravelApp.Data;
using TravelApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Repositories
{
    public class LikePostRepository : BaseRepository<LikePost>
    {
        public LikePostRepository(EntityContext ctx) : base(ctx)
        {
        }

        /// <summary>
        /// Check LikePost Exits
        /// </summary>
        /// <param name="idPost"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public int LikePostExist(int idPost, int idUser)
        {
            var arr = Model.Where(likePost => likePost.idPost == idPost && likePost.idUser == idUser).ToList();
            if (arr.Any()) { return arr.ElementAt(0).id; }
            return 0;
        }

        /// <summary>
        /// Get LikeCount of a post
        /// </summary>
        /// <param name="idPost"></param>
        /// <returns></returns>
        public int GetLikeCount(int idPost)
        {
            return Model.Where(likePost => likePost.idPost == idPost).Count();
        }

        /// <summary>
        /// Delete likePost when a post 's deleted
        /// </summary>
        /// <param name="idPost"></param>
        public void DeleteByIdPost(int idPost)
        {
            foreach (var c in Model.Where(likePost => likePost.idPost == idPost))
            {
                DeleteEntityById(c.id);
            }
        }
    }
}
