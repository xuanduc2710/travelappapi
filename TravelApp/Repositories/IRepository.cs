﻿using TravelApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Repositories
{
    public interface IRepository<T>
    {
        BaseEntity CreateEntity(BaseEntity baseEntity);
        List<T> GetAllEntity();
        T GetEntityById(int id);
        bool UpdateEntity(int id, BaseEntity baseEntity);
        bool DeleteEntityById(int id);
    }
}
