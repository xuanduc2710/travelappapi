﻿using TravelApp.Data;
using TravelApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Repositories
{
    public class CommentRepository : BaseRepository<Comment>
    {
        public CommentRepository(EntityContext ctx) : base(ctx)
        {
        }

        /// <summary>
        /// Get Parent-Comments By Id Post
        /// </summary>
        /// <param name="idPost"></param>
        /// <returns></returns>
        public List<Comment> GetCommentParentByIdPost (int idPost)
        {
            return Model.Where(c => c.idPost == idPost && c.idParentComment == 0).OrderBy(c => c.timeCreated).Reverse().ToList();
        }

        /// <summary>
        /// Get Child-Comments of a ParentComment
        /// </summary>
        /// <param name="idParentComment"></param>
        /// <returns></returns>
        public List<Comment> GetChildComment(int idParentComment)
        {
            return Model.Where(c => c.idParentComment == idParentComment).OrderBy(c => c.timeCreated).Reverse().ToList();
        }

        /// <summary>
        /// Delete By id Post
        /// </summary>
        /// <param name="idPost"></param>
        public void DeleteByIdPost(int idPost)
        {
            var arr = Model.Where(c => c.idPost == idPost);
            foreach (var c in arr)
            {
                DeleteEntityById(c.id);
            }
        }
    }
}
