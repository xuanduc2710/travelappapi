﻿using TravelApp.Models;
using System;

namespace TravelApp.Dto
{
    public class UserDto
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string introduce { get; set; }
        public string avatarImage { get; set; }
        public string coverImage { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string address { get; set; }

        public UserDto() { }

        public UserDto(int id, string userName, string introduce, string avatarImage, string coverImage, string email, string phoneNumber, string address)
        {
            this.id = id;
            this.userName = userName;
            this.introduce = introduce;
            this.avatarImage = avatarImage;
            this.coverImage = coverImage;
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.address = address;
        }

        public UserDto(User user)
        {
            try
            {
                if(user == null)
                {
                    return;
                } else {
                    this.id = user.id;
                    this.userName = user.userName;
                    this.introduce = user.introduce;
                    this.avatarImage = user.avatarImage;
                    this.coverImage = user.coverImage;
                    this.email = user.email;
                    this.phoneNumber = user.phoneNumber;
                    this.address = user.address;
                }
            } catch(Exception exp)
            {
                throw exp;
            }
        }
    }
}
