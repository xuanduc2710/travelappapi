﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Models
{
    public class Notification : BaseEntity
    {
        public int idUser { get; set; }
        public int idUserB { get; set; }
        public DateTime timeCreated { get; set; }
        public int typeNotify { get; set; }
        public int idPost { get; set; }
        public int idComment { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idUser"></param>
        /// <param name="idUserB"></param>
        /// <param name="timeCreated"></param>
        /// <param name="typeNotify"></param>
        /// <param name="idPost"></param>
        /// <param name="idComment"></param>
        public Notification(int id, int idUser, int idUserB, DateTime timeCreated, int typeNotify, int idPost, int idComment)
        {
            this.id = id;
            this.idUser = idUser;
            this.idUserB = idUserB;
            this.timeCreated = timeCreated;
            this.typeNotify = typeNotify;
            this.idPost = idPost;
            this.idComment = idComment;
        }

        public Notification() { }
    }
}
