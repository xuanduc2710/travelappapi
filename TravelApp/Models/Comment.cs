﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Models
{
    public class Comment : BaseEntity
    {
        public string content { get; set; }
        public DateTime timeCreated { get; set; }
        public int idPost { get; set; }
        public int idParentComment { get; set; }
        public int idUser { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="content"></param>
        /// <param name="timeCreated"></param>
        /// <param name="idPost"></param>
        /// <param name="idParentComment"></param>
        /// <param name="idUser"></param>
        public Comment(int id, string content, DateTime timeCreated, int idPost, int idParentComment, int idUser)
        {
            this.id = id;
            this.content = content;
            this.timeCreated = timeCreated;
            this.idPost = idPost;
            this.idParentComment = idParentComment;
            this.idUser = idUser;
        }

        public Comment() { }
    }
}
