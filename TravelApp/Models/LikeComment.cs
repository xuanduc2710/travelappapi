﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Models
{
    public class LikeComment : BaseEntity
    {
        public int idComment { get; set; }
        public int idUser { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idComment"></param>
        /// <param name="idUser"></param>
        public LikeComment(int id, int idComment, int idUser)
        {
            this.id = id;
            this.idComment = idComment;
            this.idUser = idUser;
        }
        public LikeComment() { }
    }
}
