﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Models
{
    public class Post : BaseEntity
    {
        public int idUser { get; set; }
        public DateTime timeCreated { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public int rate { get; set; }
        public string images { get; set; }
        public string videos { get; set; }
        public int idCategory { get; set; }
        public string location { get; set; }
        public int shareCount { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idUser"></param>
        /// <param name="timeCreated"></param>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <param name="rate"></param>
        /// <param name="images"></param>
        /// <param name="videos"></param>
        /// <param name="idCategory"></param>
        /// <param name="location"></param>
        /// <param name="shareCount"></param>

        public Post(int id, int idUser, DateTime timeCreated, string title, string content, int rate, string images, string videos, int idCategory, string location, int shareCount)
        {
            this.id = id;
            this.idUser = idUser;
            this.timeCreated = timeCreated;
            this.title = title;
            this.content = content;
            this.rate = rate;
            this.images = images;
            this.videos = videos;
            this.idCategory = idCategory;
            this.location = location;
            this.shareCount = shareCount;
        }

        public Post() { }
    }
}
