﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Models
{
    public class LikePost : BaseEntity
    {
        public int idPost { get; set; }
        public int idUser { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idPost"></param>
        /// <param name="idUser"></param>
        public LikePost(int id, int idPost, int idUser)
        {
            this.id = id;
            this.idPost = idPost;
            this.idUser = idUser;
        }

        public LikePost() { }
    }
}
