﻿using System;

namespace TravelApp.Models
{
    public class UserLogin
    {
        public string email { get; set; }
        public string password { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        public UserLogin(string email, string password)
        {
            this.email = email;
            this.password = password;
        }

        public UserLogin() { }
    }
}
