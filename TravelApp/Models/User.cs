﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Models
{
    public class User : BaseEntity
    {
        public string userName { get; set; }
        public string introduce { get; set; }
        public string avatarImage { get; set; }
        public string coverImage { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string phoneNumber { get; set; }
        public string address { get; set; }

        public User() { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <param name="introduce"></param>
        /// <param name="avatarImage"></param>
        /// <param name="coverImage"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="address"></param>
        public User(int id, string userName, string introduce, string avatarImage, string coverImage, string email, string password, string phoneNumber, string address)
        {
            this.id = id;
            this.userName = userName;
            this.introduce = introduce;
            this.avatarImage = avatarImage;
            this.coverImage = coverImage;
            this.email = email;
            this.password = password;
            this.phoneNumber = phoneNumber;
            this.address = address;
        }
    }
}
