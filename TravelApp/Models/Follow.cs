﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Models
{
    public class Follow : BaseEntity
    {
        public int idFollower { get; set; }
        public int idFollowed { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idFollower"></param>
        /// <param name="idFollowed"></param>
        public Follow(int id, int idFollower, int idFollowed)
        {
            this.id = id;
            this.idFollower = idFollower;
            this.idFollowed = idFollowed;
        }
        public Follow() { }
    }
}
