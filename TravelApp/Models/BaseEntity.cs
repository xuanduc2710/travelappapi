﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Models
{
    public class BaseEntity
    {
        public int id { get; set; }
    }
}
