#pragma checksum "C:\Users\xuand\source\repos\TravelApp\TravelApp\Views\Comments\Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "59b89e406fead91876f0a62ca2b472a00bad8907"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Comments_Edit), @"mvc.1.0.view", @"/Views/Comments/Edit.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"59b89e406fead91876f0a62ca2b472a00bad8907", @"/Views/Comments/Edit.cshtml")]
    #nullable restore
    public class Views_Comments_Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TravelApp.Models.Comment>
    #nullable disable
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\xuand\source\repos\TravelApp\TravelApp\Views\Comments\Edit.cshtml"
  
    ViewData["Title"] = "Edit";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<h1>Edit</h1>

<h4>Comment</h4>
<hr />
<div class=""row"">
    <div class=""col-md-4"">
        <form asp-action=""Edit"">
            <div asp-validation-summary=""ModelOnly"" class=""text-danger""></div>
            <div class=""form-group"">
                <label asp-for=""content"" class=""control-label""></label>
                <input asp-for=""content"" class=""form-control"" />
                <span asp-validation-for=""content"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""timeCreated"" class=""control-label""></label>
                <input asp-for=""timeCreated"" class=""form-control"" />
                <span asp-validation-for=""timeCreated"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""idPost"" class=""control-label""></label>
                <input asp-for=""idPost"" class=""form-control"" />
                <span asp-validation-for=""idPost"" class=""text-danger""></span>");
            WriteLiteral(@"
            </div>
            <div class=""form-group"">
                <label asp-for=""idParentComment"" class=""control-label""></label>
                <input asp-for=""idParentComment"" class=""form-control"" />
                <span asp-validation-for=""idParentComment"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""idUser"" class=""control-label""></label>
                <input asp-for=""idUser"" class=""form-control"" />
                <span asp-validation-for=""idUser"" class=""text-danger""></span>
            </div>
            <input type=""hidden"" asp-for=""id"" />
            <div class=""form-group"">
                <input type=""submit"" value=""Save"" class=""btn btn-primary"" />
            </div>
        </form>
    </div>
</div>

<div>
    <a asp-action=""Index"">Back to List</a>
</div>

");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n");
#nullable restore
#line 53 "C:\Users\xuand\source\repos\TravelApp\TravelApp\Views\Comments\Edit.cshtml"
      await Html.RenderPartialAsync("_ValidationScriptsPartial");

#line default
#line hidden
#nullable disable
            }
            );
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TravelApp.Models.Comment> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
