﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TravelApp.Data;
using TravelApp.Repositories;
using TravelApp.Response;
using Microsoft.AspNetCore.Authorization;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ScreenPostDetailController : ControllerBase
    {
        private readonly PostRepository _postRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ScreenPostDetailController(EntityContext context)
        {
            _postRepository = new PostRepository(context);
        }

        /// <summary>
        /// Get Post Response (Post Detail Screen)
        /// </summary>
        /// <param name="idPost"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        [HttpGet("idPost={idPost}")]
        public async Task<ActionResult<PostResponse>> GetPostResponsesBy(int idPost, int idUser)
        {
            return new PostResponse(_postRepository.GetEntityById(idPost), idUser);
        }

    }
}
