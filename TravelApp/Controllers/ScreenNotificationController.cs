﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TravelApp.Data;
using TravelApp.Response;
using Microsoft.AspNetCore.Authorization;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ScreenNotificationController : ControllerBase
    {

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ScreenNotificationController(EntityContext context)
        {
        }

        /// <summary>
        /// Get Notification Response (Notification Screen)
        /// </summary>
        /// <param name="idUser"></param>
        /// <returns></returns>
        [HttpGet("idUser={idUser}")]
        public async Task<ActionResult<NotificationScreenResponse>> GetNotificationScreenResponse(int idUser)
        {
            return new NotificationScreenResponse(idUser);
        }
    }
}
