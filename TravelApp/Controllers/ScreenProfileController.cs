﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TravelApp.Data;
using TravelApp.Response;
using Microsoft.AspNetCore.Authorization;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ScreenProfileController : ControllerBase
    {

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ScreenProfileController(EntityContext context)
        {
        }

        /// <summary>
        /// Get Profile Response (Profile Screeen)
        /// </summary>
        /// <param name="idProfile"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        [HttpGet("idProfile={idProfile}")]
        public async Task<ActionResult<ProfileResponse>> GetProfileResponse(int idProfile, int idUser)
        {
            return new ProfileResponse(idProfile, idUser);
        }
    }
}
