﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;
using TravelApp.Response;
using TravelApp.Dto;
using Microsoft.AspNetCore.Authorization;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IConfiguration _config;
        private readonly UserRepository userRepository;

        /// <summary>
        /// Constructor LoginController
        /// </summary>
        /// <param name="context"></param>
        /// <param name="configuration"></param>
        public LoginController(EntityContext context, IConfiguration configuration)
        {
            this.userRepository = new UserRepository(context);
            this._config = configuration;
        }

        /// <summary>
        /// User Login
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("UserLogin")]
        public async Task<ActionResult<LoginResponse>> UserLogin(UserLogin userLogin)
        {
            LoginResponse result = new LoginResponse();
            User user = userRepository.CheckLogin(userLogin);

            if (user != null)
            {
                result.token = GetToken(user);
                UserDto userDto = new UserDto(user);
                result.user = userDto;
                return result;
            }
            return NotFound("User not found!");
        }

        /// <summary>
        /// get Token by User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private string GetToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JWT:Secret"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var claimList = new[]
            {
                new Claim("id", user.id.ToString()),
                new Claim("email", user.email.ToString()),
                new Claim("name", user.userName.ToString()),
                new Claim("avartar", user.avatarImage.ToString()),
                new Claim("introduce", user.introduce.ToString()),
                new Claim("numPhone", user.phoneNumber.ToString()),
                new Claim("address", user.address.ToString())
            };
            var token = new JwtSecurityToken(
                issuer: _config["JWT:ValidIssuer"],
                audience: _config["JWT:ValidAudience"],
                expires: DateTime.Now.AddDays(1),
                claims: claimList,
                signingCredentials: credentials
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
