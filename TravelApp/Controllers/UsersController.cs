﻿using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;
using TravelApp.Dto;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly UserRepository _userRepository;

        /// <summary>
        /// constructor userscontroller
        /// </summary>
        /// <param name="context"></param>
        public UsersController(EntityContext context)
        {
            _userRepository = new UserRepository(context);
        }

        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetAll()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            
            return _userRepository.GetAllEntity();
        }

        /// <summary>
        /// Update Profile
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProfile(int id, UserDto userDto)
        {
            try
            {
                User user = _userRepository.GetEntityById(id);
                user.userName = userDto.userName;
                user.introduce = userDto.introduce;
                user.coverImage = userDto.coverImage;
                user.avatarImage = userDto.avatarImage;
                user.phoneNumber = userDto.phoneNumber;
                user.address = userDto.address;

                _userRepository.UpdateEntity(id, user);
            }
            catch(Exception exp)
            {
                throw exp;
            }
            return NoContent();
        }

        /// <summary>
        /// Update Password
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        [HttpPut("NewPassword")]
        public async Task<IActionResult> UpdatePassword(int id, UserLogin userLogin)
        {
            try
            {
                User user = _userRepository.GetEntityById(id);
                user.password = userLogin.password;

                _userRepository.UpdateEntity(id, user);
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return NoContent();
        }

        /// <summary>
        /// Create new User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<User>> Create(User user)
        {
            try
            {
                if (!_userRepository.CheckAccountExist(user.email) && user.email != "" && user.password != "" && user.userName != "")
                {
                    return _userRepository.CreateEntity(user) as User;
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
    }
}
