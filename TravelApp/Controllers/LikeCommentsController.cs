﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LikeCommentsController : ControllerBase
    {
        private readonly LikeCommentRepository _likeCommentRepository;
        private readonly CommentRepository _commentRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly PostRepository _postRepository;
        private readonly UserRepository _userRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public LikeCommentsController(EntityContext context)
        {
            _likeCommentRepository = new LikeCommentRepository(context);
            _commentRepository = new CommentRepository(context);
            _notificationRepository = new NotificationRepository(context);
            _postRepository = new PostRepository(context);
            _userRepository = new UserRepository(context);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<LikeComment>>> GetAll()
        {
            return _likeCommentRepository.GetAllEntity();
        }

        /// <summary>
        /// Unlike Comment
        /// </summary>
        /// <param name="idPost"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> UnLike(int idComment, int idUser)
        {
            try
            {
                if (_commentRepository.EntityExist(idComment) && _userRepository.EntityExist(idUser) && _likeCommentRepository.LikeCommentExist(idComment, idUser) != 0)
                {
                    /// Delete LikeComment and Delete notification like comment
                    _likeCommentRepository.DeleteEntityById(_likeCommentRepository.LikeCommentExist(idComment, idUser));
                    _notificationRepository.DeleteEntityById(_notificationRepository.NotificationExist(_postRepository.GetEntityById(_commentRepository.GetEntityById(idComment).idPost).idUser,
                        idUser, 3, _commentRepository.GetEntityById(idComment).idPost, idComment));
                    return NoContent();
                } else
                {
                    return NotFound("LikeComment Not Exist!");
                } 
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }


        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="likeComment"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<LikeComment>> Create(LikeComment likeComment)
        {
            try
            {
                if(_likeCommentRepository.LikeCommentExist(likeComment.idComment, likeComment.idUser) == 0 &&
                    _commentRepository.EntityExist(likeComment.idComment) &&
                    _userRepository.EntityExist(likeComment.idUser))
                {
                    /// Create new likeComment
                    var newLikeComment = _likeCommentRepository.CreateEntity(likeComment) as LikeComment;

                    /// Create Notification
                    Notification _notification = new Notification();
                    _notification.idUser = _postRepository.GetEntityById(_commentRepository.GetEntityById(likeComment.idComment).idPost).idUser; ;
                    _notification.idPost = _commentRepository.GetEntityById(likeComment.idComment).idPost;
                    _notification.idComment = likeComment.idComment;
                    _notification.idUserB = likeComment.idUser;
                    _notification.timeCreated = DateTime.Now;
                    _notification.typeNotify = 3;
                    Notification notification = _notificationRepository.CreateEntity(_notification) as Notification;

                    return newLikeComment;
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
    }
}
