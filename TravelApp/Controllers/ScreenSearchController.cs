﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TravelApp.Data;
using TravelApp.Response;
using Microsoft.AspNetCore.Authorization;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ScreenSearchController : ControllerBase
    {

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ScreenSearchController(EntityContext context)
        {
        }

        /// <summary>
        /// Get Search Response (Search Screen)
        /// </summary>
        /// <param name="keySearch"></param>
        /// <param name="typeSearch"></param>
        /// <param name="idUser"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet("keySearch={keySearch}")]
        public async Task<ActionResult<SearchResponse>> GetSearchResponse(string keySearch, int idUser, int pageUser, int pagePost)
        {
            return new SearchResponse(keySearch, idUser, pageUser, pagePost);
        }
    }
}
