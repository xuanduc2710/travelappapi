﻿using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostsController : ControllerBase
    {
        private readonly PostRepository _postRepository;
        private readonly CommentRepository _commentRepository;
        private readonly LikeCommentRepository _likeCommentRepository;
        private readonly DislikePostRepository _dislikePostRepository;
        private readonly LikePostRepository _likePostRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly UserRepository _userRepository;
        private readonly FollowRepository _followRepository;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="context"></param>
        public PostsController(EntityContext context)
        {
            _postRepository = new PostRepository(context);
            _commentRepository = new CommentRepository(context);
            _dislikePostRepository = new DislikePostRepository(context);
            _likeCommentRepository = new LikeCommentRepository(context);
            _likePostRepository = new LikePostRepository(context);
            _notificationRepository = new NotificationRepository(context);
            _userRepository = new UserRepository(context);
            _followRepository = new FollowRepository(context);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Post>>> GetAll()
        {
            return _postRepository.GetAllEntity();
        }

        /// <summary>
        /// Increase share count
        /// </summary>
        /// <param name="idPost"></param>
        /// <returns></returns>
        [HttpPut("idPostIncreaseShareCount={idPostIncreaseShareCount}")]
        public async Task<ActionResult<Post>> IncreaseShareCount(int idPostIncreaseShareCount)
        {
            try
            {
                return _postRepository.IncreaseShareCount(idPostIncreaseShareCount);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Update API
        /// </summary>
        /// <param name="id"></param>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, Post post)
        {
            try
            {
                _postRepository.UpdateEntity(id, post);
            }
            catch
            {
                throw;
            }
            return NoContent();
        }

        /// <summary>
        /// Create new Post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Post>> Create(Post post)
        {
            try
            {
                if (post.title == "" || post.content == "" ||
                    post.rate == 0 || post.idCategory == 0) return NotFound("Title, Content is not allow null; Rate, IdCategory is not allow 0;");

                if (_userRepository.EntityExist(post.idUser)) {
                    post.timeCreated = DateTime.Now;
                    post.shareCount = 0;
                    Post newPost = _postRepository.CreateEntity(post) as Post;

                    foreach(int idFollower in _followRepository.GetListIdFollower(post.idUser))
                    {
                        Notification _notification = new Notification();
                        _notification.idUser = idFollower;
                        _notification.idPost = post.id;
                        _notification.idComment = 0;
                        _notification.idUserB = post.idUser;
                        _notification.timeCreated = DateTime.Now;
                        _notification.typeNotify = 6;
                        Notification notification = _notificationRepository.CreateEntity(_notification) as Notification;
                    }
                    
                    return newPost;
                } else
                {
                    return NotFound();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Delete Post
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                ///Delete post
                _postRepository.DeleteEntityById(id);

                /// Delete likepost, disLikePost
                _likePostRepository.DeleteByIdPost(id);
                _dislikePostRepository.DeleteByIdPost(id);

                /// Delete all notification relate this post
                _notificationRepository.DeleteByIdPost(id);

                /// Delete comment, likeComment
                foreach (Comment comment in _commentRepository.GetCommentParentByIdPost(id))
                {
                    _commentRepository.DeleteEntityById(id);
                    _likeCommentRepository.DeleteByIdComment(id);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return NoContent();
        }
    }
}
