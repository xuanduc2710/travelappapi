﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LikePostsController : ControllerBase
    {
        private readonly LikePostRepository _likePostRepository;
        private readonly DislikePostRepository _dislikePostRepository;
        private readonly PostRepository _postRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly UserRepository _userRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public LikePostsController(EntityContext context)
        {
            _likePostRepository = new LikePostRepository(context);
            _dislikePostRepository = new DislikePostRepository(context);
            _postRepository = new PostRepository(context);
            _notificationRepository = new NotificationRepository(context);
            _userRepository = new UserRepository(context);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<LikePost>>> GetAll()
        {
            return _likePostRepository.GetAllEntity();
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="likePost"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<LikePost>> Create(LikePost likePost)
        {
            try
            {
                if(_likePostRepository.LikePostExist(likePost.idPost, likePost.idUser) == 0 
                    && _postRepository.EntityExist(likePost.idPost)
                    && _userRepository.EntityExist(likePost.idUser))
                {
                    /// Delete DislikePost if exists and Delete old dislike notification
                    if(_dislikePostRepository.DislikePostExist(likePost.idPost, likePost.idUser) != 0)
                    {
                        _dislikePostRepository.DeleteEntityById(
                            _dislikePostRepository.DislikePostExist(likePost.idPost, likePost.idUser));
                        _notificationRepository.DeleteEntityById(_notificationRepository.NotificationExist(_postRepository.GetEntityById(likePost.idPost).idUser,
                            likePost.idUser, 2, likePost.idPost, 0));
                    }

                    /// Create new likePost
                    var newLikePostRepository = _likePostRepository.CreateEntity(likePost) as LikePost;

                    /// Create Notification
                    Notification _notification = new Notification();
                    _notification.idUser = _postRepository.GetEntityById(likePost.idPost).idUser;
                    _notification.idPost = likePost.idPost;
                    _notification.idComment = 0;
                    _notification.idUserB = likePost.idUser;
                    _notification.timeCreated = DateTime.Now;
                    _notification.typeNotify = 1;
                    Notification notification = _notificationRepository.CreateEntity(_notification) as Notification;

                    return newLikePostRepository;
                }
                return NotFound();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Unlike post and Delete old like notification
        /// </summary>
        /// <param name="idPost"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> UnLike(int idPost, int idUser)
        {
            try
            {
                if (_postRepository.EntityExist(idPost) && _userRepository.EntityExist(idUser) && _likePostRepository.LikePostExist(idPost, idUser) != 0){
                    _likePostRepository.DeleteEntityById(_likePostRepository.LikePostExist(idPost, idUser));
                    _notificationRepository.DeleteEntityById(_notificationRepository.NotificationExist(_postRepository.GetEntityById(idPost).idUser, idUser, 1, idPost, 0));
                    return NoContent();
                } else
                {
                    return NotFound("Likepost Not Exist!");
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
    }
}
