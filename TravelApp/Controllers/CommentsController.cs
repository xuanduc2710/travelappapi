﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CommentsController : ControllerBase
    {
        private readonly CommentRepository _commentRepository;
        private readonly LikeCommentRepository _likeCommentRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly PostRepository _postRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public CommentsController(EntityContext context)
        {
            _commentRepository = new CommentRepository(context);
            _likeCommentRepository = new LikeCommentRepository(context);
            _notificationRepository = new NotificationRepository(context);
            _postRepository = new PostRepository(context);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Comment>>> GetAll()
        {
            return _commentRepository.GetAllEntity();
        }

        /// <summary>
        /// Update API
        /// </summary>
        /// <param name="id"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, Comment comment)
        {
            if (!_commentRepository.EntityExist(id)) return NotFound("Comment not Exist!");
            try
            {
                _commentRepository.UpdateEntity(id, comment);
            }
            catch
            {
                throw;
            }
            return NoContent();
        }

        /// <summary>
        /// Delete a comment by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                if (!_commentRepository.EntityExist(id)) return NotFound("Comment not Exist!");

                _commentRepository.DeleteEntityById(id);

                /// Xoa likeComments of this comment
                _likeCommentRepository.DeleteByIdComment(id);

                /// Xoa Notifications this comment (comment, likeComment)
                _notificationRepository.DeleteByIdComment(id);
            }
            catch
            {
                throw;
            }
            return NoContent();
        }

        /// <summary>
        /// Create new Comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Comment>> Create(Comment comment)
        {
            try
            {
                if (comment.content == "") return NotFound("Content is null!");
                if (comment.idParentComment != 0 &&
                    _commentRepository.GetEntityById(comment.idParentComment).idParentComment != 0) return NotFound("It's not ParentComment!");

                /// Create Comment
                comment.timeCreated = DateTime.Now;
                var newComment = _commentRepository.CreateEntity(comment) as Comment;

                ///Create notification 
                Notification _notification = new Notification();
                _notification.idUser = _postRepository.GetEntityById(comment.idPost).idUser;
                _notification.idPost = comment.idPost;
                _notification.idComment = comment.id;
                _notification.idUserB = comment.idUser;
                _notification.timeCreated = DateTime.Now;
                _notification.typeNotify = 4;
                Notification notification = _notificationRepository.CreateEntity(_notification) as Notification;

                return newComment;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
    }
}
