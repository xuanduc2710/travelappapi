﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TravelApp.Data;
using TravelApp.Response;
using Microsoft.AspNetCore.Authorization;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ScreenHomeController : ControllerBase
    {

        /// <summary>
        /// Construcrtor
        /// </summary>
        /// <param name="context"></param>
        public ScreenHomeController(EntityContext context)
        {
        }

        /// <summary>
        /// Get Review Home Response (Home Screen)
        /// </summary>
        /// <param name="idUser"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet("page={page}")]
        public async Task<ActionResult<ReviewHomeResponse>> GetReviewHomeResponse(int idUser, int page)
        {
            if(page == 0) return new ReviewHomeResponse();
            return new ReviewHomeResponse(idUser, page);
        }
    }
}
