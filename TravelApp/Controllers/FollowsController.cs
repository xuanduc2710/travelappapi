﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FollowsController : ControllerBase
    {
        private readonly FollowRepository _followRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly UserRepository _userRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public FollowsController(EntityContext context)
        {
            _followRepository = new FollowRepository(context);
            _notificationRepository = new NotificationRepository(context);
            _userRepository = new UserRepository(context);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Follow>>> GetAll()
        {
            return _followRepository.GetAllEntity();
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="follow"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Follow>> Create(Follow follow)
        {
            try
            {
                if(_followRepository.FollowExist(follow.idFollower, follow.idFollowed) == 0 
                    && _userRepository.EntityExist(follow.idFollower)
                    && _userRepository.EntityExist(follow.idFollowed)
                    && follow.idFollowed != follow.idFollower)
                {
                    /// Create new Follow
                    var newFollow = _followRepository.CreateEntity(follow) as Follow;

                    /// Create Notification
                    Notification _notification = new Notification();
                    _notification.idUser = follow.idFollowed;
                    _notification.idPost = 0;
                    _notification.idComment = 0;
                    _notification.idUserB = follow.idFollower;
                    _notification.timeCreated = DateTime.Now;
                    _notification.typeNotify = 5;
                    Notification notification = _notificationRepository.CreateEntity(_notification) as Notification;

                    return newFollow;
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> UnFollow(int idFollower, int idFollowed)
        {
            try
            {
                if(_userRepository.EntityExist(idFollower) && _userRepository.EntityExist(idFollowed) && _followRepository.FollowExist(idFollower, idFollowed) != 0)
                {
                    _followRepository.DeleteEntityById(_followRepository.FollowExist(idFollower, idFollowed));
                    _notificationRepository.DeleteEntityById(_notificationRepository.NotificationExist(idFollowed, idFollower, 5, 0, 0));
                    _notificationRepository.DeleteWhenUnFollow(idFollower, idFollowed);
                    return NoContent();
                } else {
                    return NotFound("Follow Not Exist!");
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
    }
}
