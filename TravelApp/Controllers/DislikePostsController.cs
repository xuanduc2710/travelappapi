﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TravelApp.Data;
using TravelApp.Models;
using TravelApp.Repositories;

namespace TravelApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DislikePostsController : ControllerBase
    {
        private readonly LikePostRepository _likePostRepository;
        private readonly DislikePostRepository _dislikePostRepository;
        private readonly PostRepository _postRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly UserRepository _userRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public DislikePostsController(EntityContext context)
        {
            _likePostRepository = new LikePostRepository(context);
            _dislikePostRepository = new DislikePostRepository(context);
            _postRepository = new PostRepository(context);
            _notificationRepository = new NotificationRepository(context);
            _userRepository = new UserRepository(context);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DislikePost>>> GetAll()
        {
            return _dislikePostRepository.GetAllEntity();
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="dislikePost"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<DislikePost>> CreateDislikePost(DislikePost dislikePost)
        {
            try
            {
                if (_dislikePostRepository.DislikePostExist(dislikePost.idPost, dislikePost.idUser) == 0 
                    && _postRepository.EntityExist(dislikePost.idPost) 
                    && _userRepository.EntityExist(dislikePost.idUser))
                {
                    /// Delete LikePost if exists and Delete old like notification
                    if (_likePostRepository.LikePostExist(dislikePost.idPost, dislikePost.idUser) != 0)
                    {
                        _likePostRepository.DeleteEntityById(
                            _likePostRepository.LikePostExist(dislikePost.idPost, dislikePost.idUser));

                        _notificationRepository.DeleteEntityById(_notificationRepository.NotificationExist(_postRepository.GetEntityById(dislikePost.idPost).idUser
                            ,dislikePost.idUser, 1, dislikePost.idPost, 0));
                    }

                    /// Create new dislikePost
                    var newDislikePost = _dislikePostRepository.CreateEntity(dislikePost) as DislikePost;

                    /// Create Notification
                    Notification _notification = new Notification();
                    _notification.idUser = _postRepository.GetEntityById(dislikePost.idPost).idUser;
                    _notification.idPost = dislikePost.idPost;
                    _notification.idComment = 0;
                    _notification.idUserB = dislikePost.idUser;
                    _notification.timeCreated = DateTime.Now;
                    _notification.typeNotify = 2;
                    Notification notification = _notificationRepository.CreateEntity(_notification) as Notification;

                    return newDislikePost;
                }
                return NotFound("Post or User not Exist or LikePost Existed!");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// UnDislike post and Delete old Dislike notification
        /// </summary>
        /// <param name="idPost"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> UnDislike(int idPost, int idUser)
        {
            try
            {
                if(_postRepository.EntityExist(idPost) && _userRepository.EntityExist(idUser) && _dislikePostRepository.DislikePostExist(idPost, idUser) != 0)
                {
                    _dislikePostRepository.DeleteEntityById(_dislikePostRepository.DislikePostExist(idPost, idUser));
                    _notificationRepository.DeleteEntityById(_notificationRepository.NotificationExist(_postRepository.GetEntityById(idPost).idUser,
                        idUser, 2, idPost, 0));
                    return NoContent();
                } else
                {
                    return NotFound("Dislike Post Not Exist!");
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
    }
}
